# Containers Docker Compose

> repositorio contiendo ejemplo del uso de docker-compose

## Para empezar:
* Aegurate tener instalado docker-compose
* Para construir las imagenes de docker necesarias:
```bash
$ docker-compose build
```
* Para iniciar la aplicación
```bash
$ docker-compose up
```
* Para parar todos los servicios
```bash
$ docker-compose down
```
## Architecture Diagram
El ejemplo contiene los siguientes componentes:
* Redis: almacenamiento efimero de información
* Flask: framework de python para aplicaciones web
* Nginx Server: servidor web, utilizado para contenido frontend
* Nginx Proxy: balanceador y proxy

El diagrama a continuacion muestra como funciona la aplicación
![Diagram](Arquitectura.jpg)


### Youtube Playlist

* [Playlist de video ejemplos](https://youtube.com/playlist?list=PL8Xnw9lMxPCHGqZVnpxqAqOhzZDAZVYJL)
